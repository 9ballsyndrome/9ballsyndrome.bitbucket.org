/**
 * シェーダーに渡すattribute情報をまとめたオブジェクトクラスです。
 * @author Kentaro Kawakatsu
 */
export class TransformFeedbackVaryingObject
{
  public name:string;
  public location:number;

  constructor($name:string, $location:number)
  {
    this.name = $name;
    this.location = $location;
  }
}
