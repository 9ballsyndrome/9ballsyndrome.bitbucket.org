/**
 * @author Kentaro Kawakatsu
 */
export class GUIPanel
{
  public num:number;
  public wireframe:boolean;

  constructor()
  {
    this.num = 1000;
    this.wireframe = false;
  }
}
