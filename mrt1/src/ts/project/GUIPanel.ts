/**
 * @author Kentaro Kawakatsu
 */
export class GUIPanel
{
  public static MOVEMENT_TYPE_WAVE:string = "wave";
  public static MOVEMENT_TYPE_SPIRAL:string = "spiral";
  public static MOVEMENT_TYPE_RANDOM:string = "random";
  public static MOVEMENT_TYPE_NONE:string = "none";
  public static MOVEMENT_LIST:string[] = [
    GUIPanel.MOVEMENT_TYPE_WAVE,
    GUIPanel.MOVEMENT_TYPE_SPIRAL,
    GUIPanel.MOVEMENT_TYPE_RANDOM,
    GUIPanel.MOVEMENT_TYPE_NONE];

  public Gbuffer:Boolean;
  public num:number;
  public helper:Boolean;
  public movement:string;

  constructor()
  {
    this.Gbuffer = false;
    this.num = 10;
    this.helper = false;
    this.movement = GUIPanel.MOVEMENT_LIST[0];
  }
}
